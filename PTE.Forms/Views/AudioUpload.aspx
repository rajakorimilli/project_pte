﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="AudioUpload.aspx.cs" Inherits="PTE.Forms.Views.AudioUpload" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title></title>
</head>
<body>
    <form id="form1" runat="server">
        <div>
            Select File &nbsp;<asp:FileUpload ID="FileUploader" runat="server" /><br />
            <asp:Button ID="btnUpload" runat="server" Text="Upload"
                OnClick="btnUpload_Click" />
            <asp:GridView ID="GridView1" runat="server" AutoGenerateColumns="false" RowStyle-BackColor="#A1DCF2" Font-Names="Arial" Font-Size="10pt"
                HeaderStyle-BackColor="#3AC0F2" HeaderStyle-ForeColor="White">
                <Columns>
                    <asp:BoundField DataField="Name" HeaderText="FileName" />
                    <asp:TemplateField>
                        <ItemTemplate>
                            <object type="application/x-shockwave-flash" data='dewplayer-vol.swf?mp3=File.ashx?Id=<%# Eval("Id") %>'
                                width="240" height="20" id="dewplayer">
                                <param name="wmode" value="transparent" />
                                <param name="movie" value='dewplayer-vol.swf?mp3=File.ashx?Id=<%# Eval("Id") %>' />
                            </object>
                        </ItemTemplate>
                    </asp:TemplateField>
                    <asp:HyperLinkField DataNavigateUrlFields="Id" Text="Download" DataNavigateUrlFormatString="~/File.ashx?Id={0}" HeaderText="Download" />
                </Columns>
            </asp:GridView>
        </div>
    </form>
</body>
</html>
