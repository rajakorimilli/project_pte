﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data.SqlClient;
using System.IO;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace PTE.Forms.Views
{
    public partial class AudioUpload : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                BindGrid();
            }
        }
        protected void btnUpload_Click(object sender, EventArgs e)
        {
            try
            {
                using (BinaryReader br = new BinaryReader(FileUploader.PostedFile.InputStream))
                {
                    byte[] bytes = br.ReadBytes((int)FileUploader.PostedFile.InputStream.Length);
                    string strConnString = "Data Source=INCCSAPPSQL01;Initial Catalog=TrueCourse;Integrated Security=True;";
                    using (SqlConnection con = new SqlConnection(strConnString))
                    {
                        using (SqlCommand cmd = new SqlCommand())
                        {
                            cmd.CommandText = "insert into PTE_AudioFile(Name, ContentType, Data) values (@Name, @ContentType, @Data)";
                            cmd.Parameters.AddWithValue("@Name", Path.GetFileName(FileUploader.PostedFile.FileName));
                            cmd.Parameters.AddWithValue("@ContentType", "audio/mpeg3");
                            cmd.Parameters.AddWithValue("@Data", bytes);
                            cmd.Connection = con;
                            con.Open();
                            cmd.ExecuteNonQuery();
                            con.Close();
                        }
                    }
                }
                Response.Redirect(Request.Url.AbsoluteUri);
            }
            catch (Exception ex)
            {

            }
        }
        private void BindGrid()
        {
            string strConnString = "Data Source=INCCSAPPSQL01;Initial Catalog=TrueCourse;Integrated Security=True;";
            using (SqlConnection con = new SqlConnection(strConnString))
            {
                using (SqlCommand cmd = new SqlCommand())
                {
                    cmd.CommandText = "select Id, Name from PTE_AudioFile";
                    cmd.Connection = con;
                    con.Open();
                    GridView1.DataSource = cmd.ExecuteReader();
                    GridView1.DataBind();
                    con.Close();
                }
            }
        }
    }
}