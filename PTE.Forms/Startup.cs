﻿using Microsoft.Owin;
using Owin;

[assembly: OwinStartupAttribute(typeof(PTE.Forms.Startup))]
namespace PTE.Forms
{
    public partial class Startup {
        public void Configuration(IAppBuilder app) {
            ConfigureAuth(app);
        }
    }
}
